import Vue from 'vue'
import '@/assets/tailwind.css'
import '@fortawesome/fontawesome-free/css/all.css'
import App from './App.vue'
import VueCompositionApi from '@vue/composition-api'

Vue.use(VueCompositionApi)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
